<?php
ini_set('error_log', __DIR__ . '/php_errors_' . date("Ymd", time()) . '.log');
ini_set('log_errors', 1);
require __DIR__ . '/vendor/autoload.php';
//Маленький роутер
$opt = [
    'controller' => FILTER_SANITIZE_STRING,
    'action' => FILTER_SANITIZE_STRING,
    'id' => FILTER_VALIDATE_INT,
];
$route = filter_input_array(INPUT_GET, $opt);
$controllerName = !empty($route['controller']) ? '\controllers\\' . ucfirst($route['controller']) . 'Controller' : '\controllers\HomeController';
$actionName = !empty($route['action']) ? $route['action'] : 'test_method_select';
//вывод json
header('Content-type: application/json');
echo call_user_func([$controllerName, $actionName], $route['id']);