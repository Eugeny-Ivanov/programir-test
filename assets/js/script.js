(function ($) {
    $.getJSON("/load.php?controller=home&action=test_method_select", function (data) {
        var items = [];
        //Формирование строк таблицы. Для верстальщика будет геморойно. Но для теста пойдет.
        $.each(data, function (key, val) {
            items.push("<tr>" +
                "<th scope='row'>" + (key + 1) + "</th>" +
                "<td>" + '<button type="button" class="btn btn-link detail" data-toggle="modal" data-target="#exampleModal" data-id=' + val.id + '>' + val.name + '</button>' + "</td>" +
                "<td>" + val.id + "</td></tr>");
        });
        $(items.join("")).appendTo("#talbe-body");
        //Расставляем клики и значения в модельное окно.
        $('body').on('click', '.detail', function () {
            $.getJSON("/load.php?controller=home&action=test_method_details&id=" + $(this).data('id'), function (data) {
                $('#exampleModal #exampleModalLabel').html(data.name);
                $('#exampleModal .modal-body').html(data.name_descr);
            });
        });
    });
})(jQuery);