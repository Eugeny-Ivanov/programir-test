<?php


namespace models;


class TestModel extends \PDO
{
    /**
     * TestModel constructor.
     */
    public function __construct()
    {
        //Инициализация PDO. Подключение к БД.
        $config = require_once __DIR__ . '/../../db_config.php';
        try {
            parent::__construct($config['dsn'], $config['db_user'], $config['db_password'], [\PDO::ATTR_PERSISTENT => true]);
        } catch (\PDOException $e) {
            echo $e->getMessage();
            die();
        }
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * SELECT одной записи по id
     * @param int $id
     * @return array
     */
    public function getDetail(int $id)
    {
        $stmt = $this->prepare("select `name`, `name_descr` from `test` where `id`=?");
        $stmt->bindValue(1, $id, \PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * SELECT списка
     * @return array
     */
    public function getAll()
    {
        $stmt = $this->prepare("select `id`, `name` from `test`");
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * Заполнение таблицы тестовыми данными.
     * @return bool
     */
    public function loadTestData()
    {
        $stmt = $this->prepare("INSERT INTO `test`(`name`, `name_descr`) VALUES( ?, ?)");
        $text = file_get_contents(__DIR__ . '/../../test.txt');
        try {
            $this->beginTransaction();
            //TRUNCATE делает скрытый commit, поэтому DELETE. AUTO_INCREMENT не сбросит.
            $this->exec('DELETE FROM test');
            for ($i = 1; $i <= 50; $i++) {
                //Формирование текста и заголовка из текста. И обрезка текста если длиннее полей в таблице.
                //Конечно лучше сделать валидатор, но готовыми пользоваться нельзя, а руками писать хлопотно. Не для теста.
                $txt = mb_substr($this->quote("#$i $text"), 0, 297) . '...';
                $title = mb_substr($this->quote($text), $i, 10);
                if (!$title) {
                    $title = "#$i name";
                }
                if (!$txt) {
                    $txt = "#$i name_descr";
                }
                $stmt->bindValue(1, $title, \PDO::PARAM_STR);
                $stmt->bindValue(2, $txt, \PDO::PARAM_STR);
                $stmt->execute();
            }
            $this->commit();
        } catch (\PDOException $e) {
            $this->rollBack();
            return ['error' => true, 'message' => $e->getMessage()];
        }
        return ['code' => 200, 'update' => (boolean)$stmt->rowCount()];
    }
}