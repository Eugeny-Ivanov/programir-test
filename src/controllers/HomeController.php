<?php


namespace controllers;

use models\TestModel;

class HomeController
{

    /**
     * Данные главной страницы
     * @return false|string
     */
    public function test_method_select()
    {
        $model = new TestModel();
        return json_encode($model->getAll());
    }


    /**
     * Данные всплывающего окна
     * @param int $id
     * @return false|string
     */
    public function test_method_details(int $id)
    {
        $model = new TestModel();
        return json_encode($model->getDetail($id));
    }

    /**
     * Заполнение таблицы тестовыми данными
     */
    public function loadTestData()
    {
        try {
            (new TestModel())->loadTestData();
            header('Location: /');
        } catch (\Exception $e) {
            //Здесь запись ошибки в сессию или щдругое хранилище.
            header('Location: /');
        }
    }
}
